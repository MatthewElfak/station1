<!DOCTYPE html>
<html>
<head>
	<title>Station Fast Food</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="">
	<meta name="description" content="">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
	<link rel="icon" href="images/logic.jpg">
	<link rel="stylesheet" type="text/css" href="assets/css/home.css">
	<link rel="stylesheet" type="text/css" href="assets/css/header.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/popper.min.js"></script>
	<script src="assets/js/scroll-navbar.js"></script>
	<script src="assets/js/scroll-to-section.js"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Galada" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="assets/css/footer.css">
	<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
	<style>
		body { font-family:'Open Sans'; background-color:#fafafa;}
		h2 { margin:30px auto;}

		#mixedSlider {
		  position: relative;
		}
		#mixedSlider .MS-content {
		  white-space: nowrap;
		  overflow: hidden;
		  margin: 0 5%;
		}
		#mixedSlider .MS-content .item {
		  display: inline-block;
		  width: 33.3333%;
		  position: relative;
		  vertical-align: top;
		  overflow: hidden;
		  height: 100%;
		  white-space: normal;
		  padding: 0 10px;
		}
		@media (max-width: 991px) {
		  #mixedSlider .MS-content .item {
		    width: 50%;
		  }
		}
		@media (max-width: 767px) {
		  #mixedSlider .MS-content .item {
		    width: 100%;
		  }
		}
		#mixedSlider .MS-content .item .imgTitle {
		  position: relative;
		}
		#mixedSlider .MS-content .item .imgTitle .blogTitle {
		  margin: 0;
		  text-align: left;
		  letter-spacing: 2px;
		  color: #252525;
		  font-style: italic;
		  position: absolute;
		  background-color: rgba(255, 255, 255, 0.5);
		  width: 100%;
		  bottom: 0;
		  font-weight: bold;
		  padding: 0 0 2px 10px;
		}
		#mixedSlider .MS-content .item .imgTitle img {
		  height: auto;
		  width: 100%;
		}
		#mixedSlider .MS-content .item p {
		  font-size: 16px;
		  margin: 2px 10px 0 5px;
		  text-indent: 15px;
		}
		#mixedSlider .MS-content .item a {
		  float: right;
		  margin: 0 20px 0 0;
		  font-size: 16px;
		  font-style: italic;
		  color: rgba(173, 0, 0, 0.82);
		  font-weight: bold;
		  letter-spacing: 1px;
		  transition: linear 0.1s;
		}
		#mixedSlider .MS-content .item a:hover {
		  text-shadow: 0 0 1px grey;
		}
		#mixedSlider .MS-controls button {
		  position: absolute;
		  border: none;
		  background-color: transparent;
		  outline: 0;
		  font-size: 50px;
		  top: 95px;
		  color: rgba(0, 0, 0, 0.4);
		  transition: 0.15s linear;
		}
		#mixedSlider .MS-controls button:hover {
		  color: rgba(0, 0, 0, 0.8);
		}
		@media (max-width: 992px) {
		  #mixedSlider .MS-controls button {
		    font-size: 30px;
		  }
		}
		@media (max-width: 767px) {
		  #mixedSlider .MS-controls button {
		    font-size: 20px;
		  }
		}
		#mixedSlider .MS-controls .MS-left {
		  left: 0px;
		}
		@media (max-width: 767px) {
		  #mixedSlider .MS-controls .MS-left {
		    left: -10px;
		  }
		}
		#mixedSlider .MS-controls .MS-right {
		  right: 0px;
		}
		@media (max-width: 767px) {
		  #mixedSlider .MS-controls .MS-right {
		    right: -10px;
		  }
		}
		#basicSlider { position: relative; }

		#basicSlider .MS-content {
		  white-space: nowrap;
		  overflow: hidden;
		  margin: 0 2%;
		  height: 50px;
		}

		#basicSlider .MS-content .item {
		  display: inline-block;
		  width: 20%;
		  position: relative;
		  vertical-align: top;
		  overflow: hidden;
		  height: 100%;
		  white-space: normal;
		  line-height: 50px;
		  vertical-align: middle;
		}
		@media (max-width: 991px) {

		#basicSlider .MS-content .item { width: 25%; }
		}
		@media (max-width: 767px) {

		#basicSlider .MS-content .item { width: 35%; }
		}
		@media (max-width: 500px) {

		#basicSlider .MS-content .item { width: 50%; }
		}

		#basicSlider .MS-content .item a {
		  line-height: 50px;
		  vertical-align: middle;
		}

		#basicSlider .MS-controls button { position: absolute; }

		#basicSlider .MS-controls .MS-left {
		  top: 35px;
		  left: 10px;
		}

		#basicSlider .MS-controls .MS-right {
		  top: 35px;
		  right: 10px;
		}

		.MS-left{
			top: 100px !important;
			color: #ed1b24 !important;
		}

		.MS-right{
			top: 100px !important;
			color: #ed1b24 !important;
		}
	</style>
</head>
<body>
	
	<?php require_once 'header.php'; ?>

	<div id="slides">
	    <div class="slides-container">
	      	<img src="images/3.jpg" alt="">
	      	<img src="images/1.jpg" alt="">
	      	<img src="images/4.jpg" alt="">
	      	<img src="images/2.jpg" alt="">
	    </div>
 	</div>

	<div class="container-fluid" id="about-section">
		<div class="row">
			<div class="container">
				<div class="col-12 text-center">
					<h1>About Us</h1>
					<p>We are a Mediterranean restaurant that serves a wide selection of delicious food like Chicken & Beef Shawarma, Falafel, Chicken Kebab, and much more. We only use the highest quality meats and the freshest ingredients to ensure the same great taste every time. Our platters are delicious and filling, and mostly gluten-free. We pride ourselves on the fact that our rice, hummus, fattoush salad, and garlic sauce are all homemade. We look forward to serving all our lovely customers in the years to come!</p>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid menu-wrapper">
		<div class="container" id="menu-section">
		

	<!--//////////////////////////SALADS/////////////////////////-->

			<div class="row first-row align-items-center">
				<div class="col-2 offset-1 line text-center"></div>
				<div class="col-6 category text-center">Salads</div>
				<div class="col-2 line text-center"></div>
			</div>

			<div class="row align-items-center first-menu">
				<div class="col-8 offset-sm-0 col-sm-3 offset-md-0 col-md-3 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Greek Salad</h2><h6>Romaine lettuce, tomato, cucumber, topped with Feta cheese, olives and our special Greek house dressing</h6></div>
				<div class="col-2 col-sm-1 align-self-center price">
					<h2 class="col-12 text-left text-sm-center two-size">Sm.</h2>
					<h2 class="two-size"><span style="margin-top: 2px;">$</span>6.99</h2> 
				</div>
				<div class="col-2 col-sm-1 align-self-center price">
					<h2 class="col-12 text-left text-sm-center two-size">Lg.</h2>
					<h2 class="two-size"><span style="margin-top: 2px;">$</span>8.99</h2> 
				</div>
				<div class="col-8 offset-sm-1 col-sm-3 offset-md-1 col-md-3 offset-lg-0 col-lg-3 text-left menu-item"><h2>Taboule</h2><h6>Italian parsley, green onions and tomato mixed with lemon, mint and olive oil</h6></div>
				<div class="col-2 col-sm-1 align-self-center price">
					<h2 class="col-12 text-left text-sm-center two-size">Sm.</h2>
					<h2 class="two-size"><span style="margin-top: 2px;">$</span>6.99</h2> 
				</div>
				<div class="col-2 col-sm-1 align-self-center price">
					<h2 class="col-12 text-left text-sm-center two-size">Lg.</h2>
					<h2 class="two-size"><span style="margin-top: 2px;">$</span>8.99</h2> 
				</div>
			</div>
			<div class="row align-items-center first-menu">
				<div class="col-8 offset-sm-0 col-sm-3 offset-md-0 col-md-3 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Fatoush</h2><h6>Romaine lettuce, tomato, cucumber, red onion, mint, Somack, olive oil and special home made dressing</h6></div>
				<div class="col-2 col-sm-1 align-self-center price">
					<h2 class="col-12 text-left text-sm-center two-size">Sm.</h2>
					<h2 class="two-size"><span style="margin-top: 2px;">$</span>6.99</h2> 
				</div>
				<div class="col-2 col-sm-1 align-self-center price">
					<h2 class="col-12 text-left text-sm-center two-size">Lg.</h2>
					<h2 class="two-size"><span style="margin-top: 2px;">$</span>8.99</h2> 
				</div>
				<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-0 col-lg-4 text-left menu-item"><h2>Shawarma Bowl</h2><h6>Large Fatoush salad topped with Chicken Shawarma</h6></div>
				<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>9.99</h2></div>
			</div>


			<div class="row align-items-center first-menu">
				<div class="col-12 text-center"><h2 class="text-center addon2">Side Orders</h2></div>
				<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Hummus</h2><h6>12 oz</h6></div>
				<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>6.99</h2></div>
				<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Falafel</h2><h6>5 pieces</h6></div>
				<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>5.99</h2></div>
			</div>

			<div class="row align-items-center first-menu">
				<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Kebe</h2><h6></h6></div>
				<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>6.99</h2></div>
				<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Cheese Rolls</h2><h6></h6></div>
				<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>6.99</h2></div>
			</div>

			<div class="row align-items-center first-menu">
				<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Grape Leaves</h2><h6></h6></div>
				<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>5.99</h2></div>
				<div class="col-8 offset-sm-1 col-sm-3 offset-md-1 col-md-3 offset-lg-1 col-lg-3 text-left menu-item"><h2>Fries</h2><h6></h6></div>
				<div class="col-2 col-sm-1 align-self-center price">
					<h2 class="col-12 text-left text-sm-center two-size">Sm.</h2>
					<h2 class="two-size"><span style="margin-top: 2px;">$</span>2.99</h2> 
				</div>
				<div class="col-2 col-sm-1 align-self-center price">
					<h2 class="col-12 text-left text-sm-center two-size">Lg.</h2>
					<h2 class="two-size"><span style="margin-top: 2px;">$</span>4.99</h2> 
				</div>
			</div>

			<div class="row align-items-center first-menu">
				<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Chicken Shawarma Poutine</h2><h6></h6></div>
				<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>9.99</h2></div>
			</div>

			<!--//////////////////////////PITTA/////////////////////////-->

			<div class="row first-row align-items-center">
				<div class="col-2 offset-1 line text-center"></div>
				<div class="col-6 category text-center">Pita Wraps</div>
				<div class="col-2 line text-center"></div>
			</div>

			<div class="row align-items-center first-menu">
				<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Crispy Chicken</h2><h6>Crispy Chicken bread, lettuce, tomato, cheddar cheese and Caesar dressing</h6></div>
				<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>8.99</h2></div>
				<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Chicken Caesar</h2><h6>Juicy Chicken breast, lettuce, tomato, cheddar cheese and Caesar dressing</h6></div>
				<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>8.99</h2></div>
			</div>

			<div class="row align-items-center first-menu">
				<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Falafel</h2><h6>Tasty Falafel, lettuce, tomato, fresh pickles and tahini sauce</h6></div>
				<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>8.99</h2></div>
				<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Beef Shawarma</h2><h6>Marinated Beef with Mediterranean spices served with thini or garlic sauce</h6></div>
				<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>8.99</h2></div>
			</div>

			<div class="row align-items-center first-menu">
				<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Chicken Shawarma</h2><h6>Marinated Chicken breast with Mediterranean spices served with tahini or garlic sauce.</h6></div>
				<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>8.99</h2></div>
				<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Donair</h2><h6>Donair meat, tomatoes, and onions topped with Donair sauce</h6></div>
				<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>8.99</h2></div>
			</div>

			<!--//////////////////////////PLATES/////////////////////////-->
			

			<div class="row first-row align-items-center">
				<div class="col-2 offset-1 line text-center"></div>
				<div class="col-6 category text-center">Plates</div>
				<div class="col-2 line text-center"></div>
			</div>


			<div class="row align-items-center first-menu">
				<div class="col-12 text-center"><h2 class="text-center addon">All plates come with Hummus, salad, rice or lentil rice</h2></div>
				<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Falafel</h2><h6>Ground mixture of chickpeas, spinach, onions, garlic and Mediterranean spices served with tahini sauce. 3 Pieces</h6></div>
				<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>9.99</h2></div>
				<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Chicken Kebab</h2><h6>Tender cubed pieces of chicken breast marinated with Mediterranean spices. 2 Skewers</h6></div>
				<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>10.99</h2></div>
			</div>

			<div class="row align-items-center first-menu">
				<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Beef Shawarma</h2><h6>Marinated Beef with Mediterranean spices, served with tahini and garlic sauce</h6></div>
				<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>11.99</h2></div>
				<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Chicken Shawarma</h2><h6>Marinated Chicken breast with Mediterranean spices, served with tahini or garlic sauce</h6></div>
				<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>10.99</h2></div>
			</div>
		</div>
	</div>


	
	<div id="reviews-section"></div>
	<div class="container-fluid" id="catering-section">
		<div class="container">
			<div class="row" style="margin-bottom: 25px; color: #ed1b24;">
				<div class="col-12 text-center">
					<h1>Reviews</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div id="mixedSlider">
					  <div class="MS-content">
					      <div class="item">
					          <p>First time visiting Byblos and it will NOT be the last! The food is so very delicious, easily the best calamari we have ever had. The seafood and kabobs are prepared on an open flame, along with the vegetables, excellent flavours. And the baklava: magnificent! You have to eat here! Thanks to the staff for incredible service (two personal table visits from the chef himself).</p><br>
					          <h3 style="text-align: right; margin-right: 15px;">-- Bradley Dillman</h3>
					      </div>
					      <div class="item">
					          <p>After a long week, my husband and i ordered delivery tonight. The food was incredible, arrived hot at the door in a very short time. The staff went above and beyond to make sure our meal and our evening was perfect! Thank you Byblos for the incredible service! We will definitely come back and let everyone know how amazing our experience was.</p><br>
					          <h3 style="text-align: right; margin-right: 15px;">-- Marlene Gillis</h3>
					      </div>
					      <div class="item">
					          <p>Some of the best food offered in the city is here at Byblos restaurant. I have ate here many times and everything I have ordered off the menu never disappoints. The meat is very good quality and so much flavour! Also, this restaurant has the best hummus in the city and great vegetarian options as well. Lastly, great service and atmosphere I highly recommend you go!</p><br>
					          <h3 style="text-align: right; margin-right: 15px;">-- Omar Kadray</h3>
					      </div>
					      <div class="item">
					          <p>A friend and I ate here for the first time, last night, and both had a very pleasant experience! The service was very welcoming and professional, the food was delicious, and the restaurant itself is very clean and family-friendly! Great spot to eat at, if you haven't already!!</p><br>
					          <h3 style="text-align: right; margin-right: 15px;">-- Monique Gracie</h3>
					      </div>
					      <div class="item">
					          <p>I went yesterday for the first time at lunch with a friend and I absolutely loved it! I had a 7 month old and a 5 year old with me. The server was very friendly and accommodating to my kids. We had the chicken shawarma plate with the hummus appetizer and it was delicious!</p><br>
					          <h3 style="text-align: right; margin-right: 15px;">-- Nathalie Poirier-Schofield</h3>
					      </div>
					  </div>
					  <div class="MS-controls">
					      <button class="MS-left"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
					      <button class="MS-right"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
					  </div>
					</div>
				</div>
			</div>
			
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script> 
	<script src="assets/js/multislider.js"></script> 
	<script>
	$('#basicSlider').multislider({
				continuous: true,
				duration: 2000
			});
			$('#mixedSlider').multislider({
				duration: 750,
				interval: 3000
			});
	</script>




	
	<div class="container-fluid">
		<div class="container" id="location-section">
			<div class="row">
				<div class="col-12 text-center">
					<h1>Hours & Location</h1>
					<p class="sub-title">"Station 1" Middle Eastern Fine Food</p>

					<div class="row wrap-line">
						<div class="col-5 line"></div>
						<div class="col-2 text-center icon"><i class="fas fa-link"></i></div>
						<div class="col-5 line"></div>
					</div>

					
					<p class="info"><b>Address: </b><a style="text-decoration: none; color: #000;" target="_blank" href="https://www.google.com/maps/place/575+Main+St,+Dartmouth,+NS+B2W+6A4,+%D0%9A%D0%B0%D0%BD%D0%B0%D0%B4%D0%B0/@44.6938352,-63.5033706,17z/data=!3m1!4b1!4m5!3m4!1s0x4b5a2444c9ee53d9:0xfc380670db66e749!8m2!3d44.6938352!4d-63.5011819">575 Main Street Dartmouth, Nova Scotia</a></p>
					<p class="info"><b>Hours: </b>Monday - Thursday <b>11:00 am</b> - <b>9:00 pm</b>, Friday - Saturday <b>11:00 am</b> - <b>10:00 pm</b>, Sunday <b>12:00 am</b> - <b>8:00 pm</b></p>
				</div>
			</div>
		</div>
	</div>


	<div class="container-fluid" id="map-section">
		<div class="row">
			<div class="col-12 map">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2836.262706969196!2d-63.50337058404631!3d44.693835179099466!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4b5a2444c9ee53d9%3A0xfc380670db66e749!2zNTc1IE1haW4gU3QsIERhcnRtb3V0aCwgTlMgQjJXIDZBNCwg0JrQsNC90LDQtNCw!5e0!3m2!1ssr!2srs!4v1530802345795" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
	</div>

	 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	  <script src="assets/js/jquery.easing.1.3.js"></script>
	  <script src="assets/js/jquery.animate-enhanced.min.js"></script>
	  <script src="assets/js/jquery.superslides.js" type="text/javascript" charset="utf-8"></script>
	  <script>

	    $(function() {
	      $('#slides').superslides({
	        play: 7000
	      });
	    });
	  </script>

	<?php require_once 'footer.php'; ?>	

</body>
</html>