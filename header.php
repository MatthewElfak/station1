<div class="wrap">
	<div class="wrapper container">
	    <div class="row">
	    	<div class="col-12">
	    		<nav class="navbar navbar-expand-lg">
				  	<a class="navbar-brand" href="#"><img id="mobile-logo" class="img-fluid" src="images/station.png"></a>
				  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				    <span class="navbar-toggler-icon"></span>
				  	</button>
				  	<div class="collapse navbar-collapse" id="navbarNav">
					    <ul class="navbar-nav ml-auto">
					      	<li class="nav-item active text-center" onclick="scrollIt('about-section')">
					        	<a class="nav-link" href="#">About</a>
					      	</li>
					      	<li class="nav-item text-center" onclick="scrollIt('menu-section')">
				       	 		<a class="nav-link" href="#">Menu</a>
					      	</li>
					      	<li class="nav-item text-center" onclick="scrollIt('reviews-section')">
				       	 		<a class="nav-link" href="#">Reviews</a>
					      	</li>
					      	<li class="nav-item text-center" onclick="scrollIt('location-section')">
					        	<a class="nav-link" href="#">Hours & Location</a>
					      	</li>
					      	<li style="margin-left: -10px" class="nav-item text-center">
					        	<a target="_blank" href="https://www.facebook.com/station1me/"><i class="fab fa-facebook"></i></a>
					        	<a href="https://www.instagram.com/explore/locations/1327061570710263/station1-middle-eastern-fine-food/" target="_blank"><i class="fab fa-instagram"></i></a>
					      	</li>
					      	<li class="nav-item text-center">
					      		<a class="nav-link" href="tel:9024462547">902-446-2547</a>
					      	</li>
					    </ul>
				  	</div>
				</nav>
	    	</div>
	    </div>
	</div>
</div>

		
<script>
	$(document).ready(function(){
		var sirina = $(window).width();

		if(sirina<576){
			$('.navbar-toggler').css('padding','0.2rem 0.45rem');
			$('.navbar-toggler').css('font-size','1.05rem');
			$('.wrap img').css('width','60%');
		}

		else if(sirina<767){
			$('.wrap img').css('width','80%');
		}


	});
</script>
			
		
						
					